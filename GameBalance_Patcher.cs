﻿using System;
using Harmony;
using System.Reflection;

namespace AddRecipe
{

    [HarmonyPatch(typeof(GameBalance))]
    [HarmonyPatch("CreateCraftsCache")]
    internal class GameBalance_CreateCraftsCache_Patch
    {

        [HarmonyPrefix]
        public static bool Prefix(GameBalance __instance)
        {

            // Create the recipe as a CraftDefinition
            CraftDefinition craftable = new CraftDefinition();
            craftable.id = "bait_steel";
            craftable.craft_in.Add("mf_jewelry");
            craftable.needs.Add(new Item("ingot_steel", 1));
            craftable.output.Add(new Item("bait_steel", 1));
            craftable.output.Add(new Item("r", 2));
            craftable.craft_time = SmartExpression.ParseExpression("2");
            craftable.energy = SmartExpression.ParseExpression("2");
            craftable.sanity = SmartExpression.ParseExpression("0");
            craftable.needs_unlock = false;
            craftable.icon = "i_bait_steel";
            craftable.craft_type = CraftDefinition.CraftType.None;

            craftable.hidden = false;
            craftable.is_auto = false;
            craftable.one_time_craft = false;
            craftable.force_multi_craft = false;
            craftable.disable_multi_craft = false;

            // Add the recipe to craft_data
            __instance.craft_data.Add(craftable);

            // Go on to run the normal CreateCraftsCache method
            return true;

        }

    }
}
