﻿
using System.Reflection;
using Harmony;

namespace AddRecipe
{
    public class MainPatcher
    {
        public static void Patch()
        {
            var harmony = HarmonyInstance.Create("com.gena.graveyardkeeper.addrecipe.mod");   // Change this line to match your mod. 
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }

    }
}
